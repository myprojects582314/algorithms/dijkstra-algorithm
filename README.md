# Physical model of thermal expansion of selected materials
> Implementation of Dijkstra's algorithm in Python

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
- This project was realized on III semester of studies.
- Program include Dijkstra algorithm and testing function in random graphs.
- The purpose of project was to learn Python language and algorithms.


## Technologies Used
- Python - version 3.9
- PyCharm Community Edition


## Features
Provided features:
- Finding shortest path in graph using Dijkstra algorithm
- Description of algorithm and result of tests
- Calculate computational complexity based on tests


## Project Status
Project is: _no longer being worked on_. 


## Acknowledgements
- This project was inspired by other implementations of this algorithm


## Contact
Created by Piotr Hajduk

