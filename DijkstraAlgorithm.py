import networkx as nx
import random


def najm_wart(s_kosztu, l_poprzednich):
    min_koszt = float('inf')
    min_nazwa = None
    for nazwa, koszt in s_kosztu.items():
        if nazwa not in l_poprzednich and min_koszt > koszt:
            min_koszt = koszt
            min_nazwa = nazwa
    return min_nazwa


def Algorytm_Dijk(graf, start):
    koszt_dotarcia = {}
    poprzedni_wierzchlek = {}
    przetworzone_wierzcholki = []

    for punkt in graf.keys():
        koszt_dotarcia[punkt] = float('inf')

    koszt_dotarcia[start] = 0
    poprzedni_wierzchlek[start] = None

    while start:
        for mozliwe_wierzcholki in graf[start]:
            if koszt_dotarcia[start] + graf[start][mozliwe_wierzcholki] < koszt_dotarcia[mozliwe_wierzcholki]:
                koszt_dotarcia[mozliwe_wierzcholki] = koszt_dotarcia[start] + graf[start][mozliwe_wierzcholki]
                poprzedni_wierzchlek[mozliwe_wierzcholki] = start
        przetworzone_wierzcholki.append(start)
        start = najm_wart(koszt_dotarcia, przetworzone_wierzcholki)


def time_alg():
    import time
    poczatek_t = time.time()
    testy = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]
    file = open('wyniki_v4.txt', 'w')
    for test in testy:
        suma_odch = 0
        suma = 0
        czas = []
        for i in range(50):
            G = nx.random_regular_graph(10, test)
            Graf = {
                n: {
                    nbr: random.randint(1, 300) for nbr, dd in nbrdict.items()
                }
                for n, nbrdict in G.adj.items()
            }
            poczatek = list(Graf.keys())[0]
            tp1 = time.time_ns()
            Algorytm_Dijk(Graf, poczatek)
            tk1 = time.time_ns()
            t1 = (tk1 - tp1) / (10 ** 9)
            if t1 != 0:
                czas.append(t1)
        print(czas)
        for i in czas:
            suma = suma + i
        wynik_sr = suma / len(czas)
        for i in czas:
            odch_kw = (i - wynik_sr) ** 2
            suma_odch = suma_odch + odch_kw
        odchylenie = (suma_odch / len(czas)) ** (1 / 2)
        file.write(f'Sredni czas wykonania  algorytmu dla testu N={test} wynosi\t{round(wynik_sr, 10)}\n')
        file.write(f'Odchylenie standardowe dla testu N={test} wynosi\t{round(odchylenie, 10)}\n\n')
        print(f'Sredni czas wykonania  algorytmu dla testu N={test} wynosi\t{round(wynik_sr, 10)}')
        print(f'Odchylenie standardowe dla testu N={test} wynosi\t{round(odchylenie, 10)}')
    koniec = time.time()
    print(koniec - poczatek_t)
    file.write(f'Czas obliczenia wszystkich testow wynosi {koniec - poczatek_t}')
    file.close()


time_alg()
